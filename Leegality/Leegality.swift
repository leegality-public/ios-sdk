//
//  Leegality.swift
//  Leegality
//
//  Created by Prakhar Agrawal on 22/01/18.
//  Copyright © 2018 Grey Swift Private Limited. All rights reserved.
//
import Foundation
import UIKit
import WebKit

public protocol LeegalityProtocol {
    func getResult(value : [String : String])
    func getAlert(value : [String : String])
}

public class Leegality: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    public var url: String!
    public var delegate: LeegalityProtocol?
    public var loadSpinner: UIActivityIndicatorView!
    
    var webView: WKWebView = {
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.preferences = preferences
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        return webView
    }()
    
    public init(url : String) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        var requestUrl: URL
        let character: Character = "?"
        if(url.contains(character)){
            requestUrl = URL(string: url+"&device=newios")!
        } else {
            requestUrl = URL(string: url+"?device=newios")!
        }
        let requestObj = URLRequest(url: requestUrl)
        loadSpinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        let transform:CGAffineTransform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        loadSpinner.transform = transform;
        loadSpinner.center = view.center
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.load(requestObj)
        configureButtons()
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        }
        self.view.addSubview(webView)
        self.view.addSubview(loadSpinner)
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = view.bounds
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loadSpinner.startAnimating()
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadSpinner.stopAnimating()
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loadSpinner.stopAnimating()
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        loadSpinner.stopAnimating()
    }

    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        var action: WKNavigationActionPolicy?
        guard let url = navigationAction.request.url else { return }
        let scheme = url.scheme
        if scheme == "leegality" {
            decisionHandler(action ?? .cancel)
            let string = url.absoluteString
            let strings = string.components(separatedBy: "://")[1].removingPercentEncoding!.components(separatedBy: ":")
            var dict = [String: String]()
            dict[strings[0]] = strings[1]
            delegate?.getResult(value: dict)
            self.dismiss(animated: true, completion: nil)
        }
        else if scheme == "leegalitydownload" {
            decisionHandler(action ?? .allow)
            let string = url.absoluteString
            let strings = string.components(separatedBy: "://")[1].removingPercentEncoding!.components(separatedBy: ":")
            let name = strings[0]
            let file  = strings[1]
            savePdf(name,file)
        }
        else if scheme == "triggerkeplerevent" {
            decisionHandler(action ?? .cancel)
            let string = url.absoluteString
            var strings = string.components(separatedBy: "://")[1].removingPercentEncoding!.components(separatedBy: ":")
            var dict = [String: String]()
            let name = strings.removeFirst()
            let jsonString = strings.joined()
            dict[name] = jsonString
            delegate?.getAlert(value: dict)
        }
        else{
            decisionHandler(action ?? .allow)
        }
    }
    
    private func configureButtons() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapButton))
    }

    @objc private func didTapButton() {
        delegate?.getResult(value: ["message":"Cancelled."])
        dismiss(animated: true, completion: nil)
    }
    
    func savePdf(_ name : String, _ base64File: String) {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
        let pdfDoc = NSData(base64Encoded: base64File, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
        fileManager.createFile(atPath: paths as String, contents: pdfDoc as Data?, attributes: nil)
        if fileManager.fileExists(atPath: paths){
            let documento = NSData(contentsOfFile: paths)
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            present(activityViewController, animated: true, completion: nil)
         } else {
             print("document was not found")
         }
    }
}
