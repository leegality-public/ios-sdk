### Steps to Use ###

1.  Use following pod profile: 
	<br __>pod ‘Leegality’, :git => ‘https://gitlab.com/leegality-public/ios-sdk’, :tag => ‘1.1.4’
2. Import Leegality on the top of the class.
3. Extend LeegalityProtocol in your ViewController class to get the response as result.
4. Create getResult method of LeegalityProtocol as shown below:
	```
	func getResult(value: [String : String]) {
		//your code here
		print(value)
	}
	```
5. Create getAlert method of LeegalityProtocol as shown below:
	```
	func getAlert(value: [String : String]) {
		//your code here
		print(value)
	}
	```
6. To start the signing process, follow the below code:
	```
	let controller = Leegality(url: "your-signing-url")
	controller.delegate = self
	let navVC = UINavigationController(rootViewController: controller)
	self.present(navVC, animated: true, completion: nil)
	```

	To customise the duration for which the success screen on the URL remains open before automatically closing down, you can append the URL with the
	query parameter “timer”.  The default value is 5 and you can pass any integer value between 0 to 60.  For example, to set the duration at 0
	seconds, pass the timer parameter with value 0 as follows:

	https://app1.leegality.com/sign/ea23fced-36e4-4fe3-ae32-181a09c2f13a?timer=0


### Result: ###

1. In case of successfully completion of signing process, signing window will be closed automatically and you will get a response with key name 'message' and corresponding 'success message' in the getResult method.
2. In case of error in signing process, signing window will be closed automatically and you will get a response with key name 'error' and corresponding 'error message'.

### Alert: ###

All the events triggered which pops up in the front end will be received in this getAlert callback method in string format which can then be parsed into JSON. The structure of the JSON is mentioned below.

### JSON Structure: ###

Broadcast event JSON structure:

```
{
	"invitationId":"string",
	"eventName":"string",
	"sessionId":"string",
	"data":{
		"notificationRaised":{
			"messages":[
			{
				"code":"string",
				"message":"string"
			}],
		"type":"string"
		}
	}
}
```
