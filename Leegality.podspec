Pod::Spec.new do |s|

  s.name         = "Leegality"
  s.version      = "1.1.4"
  s.summary      = "Leegality framework to integrate eSigning functionality in iOS apps."
  s.homepage     = "https://www.leegality.com"
  s.license      = "MIT"
  s.author       = "Prakhar Agrawal"
  s.platform     = :ios, "11.0"
  s.source       = { :git => "http://gitlab.leegality.com/leegality-public/ios-sdk.git", :tag => "1.1.4" }
  s.source_files     = "Leegality", "Leegality/**/*.{h,m,swift}"

end
